from dataclasses import fields
from distutils.command.upload import upload
from email.policy import default
import re
from unicodedata import category
from warnings import filters
import django
from django.db import models
from django.urls import reverse

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=100)        

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('category', kwargs={'category_id': self.pk})    

class Product(models.Model):
    name = models.CharField(max_length=100)
    brand = models.CharField(max_length=70)
    image = models.ImageField(upload_to ='media',blank=False,max_length=100,default='default.jpg' )
    description = models.TextField()
    price = models.DecimalField(max_digits=10,decimal_places=2)
    category = models.ForeignKey(Category,on_delete=models.DO_NOTHING,default='uncategorised',null=True)

    

    def __str__(self):
        return self.name


    def get_absolute_url(self):
        return reverse('product_detail', args=[str(self.id)])