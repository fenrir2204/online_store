from unicodedata import name
from django.conf import settings

from django.conf.urls.static import static

from django.urls import path

from .views import (WelcomePageView,
                    HomePageView,
                    ProductDetailView,
                    SearchResultListView,
                    charge)



urlpatterns = [
    path('', WelcomePageView.as_view(),name='Welcome'),
    path('store/',HomePageView.as_view(),name="home"),
    path('<int:pk>', ProductDetailView.as_view(),name='product_detail'),
    path('/charge', charge, name='charge'),
    path('search/',SearchResultListView.as_view(),name='search_results'),
]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)